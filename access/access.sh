#!/usr/bin/env bash

access_token=$1
resource=$2
scope=${3:-get}
realm=${4:-ota-users}

curl --verbose -X POST \
https://kc-dev.int.toradex.com/auth/realms/${realm}/protocol/openid-connect/token \
-H "Authorization: Bearer ${access_token}" \
--data "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket" \
--data "audience=ota-users-resource-server" \
--data "permission=${resource}#${scope}" \
--data "response_mode=decision"
  
