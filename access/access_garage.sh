#!/usr/bin/env bash

access_token=$1
resource=$2
scope=${3:-get}

curl --verbose -X POST \
https://kc-dev.int.toradex.com/auth/realms/garage-tools/protocol/openid-connect/token \
-H "Authorization: Bearer ${access_token}" \
--data "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket" \
--data "audience=garage-tools-resource-server" \
--data "permission=${resource}#${scope}" \
--data "response_mode=decision"
  
