#!/usr/bin/env bash
code=$1

curl --verbose https://kc-pilot.torizon.io/auth/realms/ota-users/protocol/openid-connect/token \
--data-urlencode client_id=bens-custom-api \
--data-urlencode code=${code} \
--data-urlencode grant_type=authorization_code \
--data-urlencode redirect_uri=https://app-pilot.torizon.io/login \
--data-urlencode client_secret=ced659d6-8ecd-4fd0-b914-4cd6dc942059 -o tokens.json
