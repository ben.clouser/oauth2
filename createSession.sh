#!/usr/bin/env bash

export JWT_TOKEN=$1

curl --verbose -H "Authorization: Bearer $JWT_TOKEN" -X POST -H "Content-Type: Application/Json" --data '{"type":"provision-device"}' https://app-dev2.int.toradex.com/api/accounts/users/sessions


