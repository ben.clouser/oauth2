#!/usr/bin/env bash

export JWT_TOKEN=$1
SESSION_ID=$2

curl --verbose -H "Authorization: Bearer $JWT_TOKEN" https://app-dev2.int.toradex.com/api/accounts/users/sessions/${SESSION_ID} -X DELETE
