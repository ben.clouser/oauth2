REFRESH_TOKEN=$1

curl --verbose --request POST \
  --url 'https://kc-dev.int.toradex.com/auth/realms/ota-users/protocol/openid-connect/token' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data 'grant_type=refresh_token' \
  --data 'client_id=provision-device' \
  --data refresh_token=$REFRESH_TOKEN
