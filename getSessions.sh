#!/usr/bin/env bash

export JWT_TOKEN=$1

curl --verbose -H "Authorization: Bearer $JWT_TOKEN" https://app-dev2.int.toradex.com/api/accounts/users/sessions
